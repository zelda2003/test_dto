export declare type Initiative = {
    init_no: number;
    init_id: string;
    init_name: string;
    company_code: string;
    owner_id: string;
    process_type: string;
    scenario_id: string;
    init_status: string;
    current_status: string;
    created_at: Date;
    updated_at: Date;
};

export enum Current_Status {
    Draft = 'Draft',
    WaitingforUpdate = 'Wait for Update',
    WaitforApproval = 'Wait for Approval',
    Rejected = 'Rejected',
    Approved = 'Approved',
    Revising = 'Revising',
    Cancelled = 'Cancelled',
}

export enum Init_Status {
    Inprogress = 'Inprogress',
    Cancelled = 'Cancelled',
    Completed = 'Completed',
}
