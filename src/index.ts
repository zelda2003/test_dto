import { IsEnum, IsNotEmpty, IsOptional, IsString, IsNumber } from 'class-validator';
import { Init_Status, Current_Status } from './interface';

export class UpdateSaleOrderDto {
    @IsNotEmpty()
    saleorder_id?: string;

    @IsString()
    ref_contract_no?: string;

    @IsNotEmpty()
    saleman?: string;

    @IsNotEmpty()
    org_name?: string;

    @IsNotEmpty()
    saleorder_date?: Date;

    @IsString()
    ref_quotation_no?: string;

    @IsString()
    currency?: string;

    @IsNotEmpty()
    export_type?: string;

    @IsString()
    ref_purchase_no?: string;

    @IsNumber()
    cost_rate?: string | number;

    @IsString()
    unit?: string;

    @IsNotEmpty()
    init_id?: string;

    @IsOptional()
    @IsEnum(Init_Status)
    init_status: string;

    @IsOptional()
    @IsEnum(Current_Status)
    current_status: string;
}

console.log(UpdateSaleOrderDto)