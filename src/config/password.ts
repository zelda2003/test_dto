export const SECRET = 'sha512';
export const SALT = '10';
export const NUMBER = 100000;
export const NUMBER_2 = 512;
export const ALGORITHM = 'sha512';
