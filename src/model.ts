import { Model, ModelObject } from 'objection';


export default class SalesmanModel extends Model {
  id: number;
  salesman_code: string;
  salesman_name_eng: string;
  salesman_name_tha: string;
  sap_code: string;
  active_flag: boolean;
  created_at: string;
  updated_at: string;
  created_by: string;
  updated_by: string;

  static tableName = 'salesman';
  static idColumn = 'id';





  static jsonSchema = {
    type: 'object',
    required: ['salesman_code'],
    properties: {
      id: { type: 'integer' },
      salesman_code: { type: 'string' },
      salesman_name_eng: { type: 'string' },
      salesman_name_tha: { type: 'string' },
      sap_code: { type: 'string' },
      active_flag: { type: 'boolean' },
      created_at: { type: 'string' },
      updated_at: { type: 'string' },
      created_by: { type: 'string' },
      updated_by: { type: 'string' },
    },
  };


}

export type SalesmanModelShape = ModelObject<SalesmanModel>;

export interface SalesmanFilter {
  id?: number;
  salesman_code?: string;
  salesman_name_eng?: string;
  salesman_name_tha?: string;
  limit?: number;
  offset?: number;
  full?: string;
}
